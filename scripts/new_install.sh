#!/bin/bash

# an install script for new linux systems.

if [ $USER == "root"  ] 
then
	echo "Do not run as root or with sudo." # if you do, everything will be done as root and you can't access anything
	exit
fi

# Confirm
read -p "Are you sure you would like to continue? [y/n]" answer
if [ $answer == "y" ]
then
	echo "Beginning install."
else
	echo "Exiting install."
fi

echo ""
echo "Installing useful packages..."
sudo apt-get install build-essential
sudo apt-get install python3 python-dev python3-dev ruby ssh openssh-server vim tmux git w3m mosh

# need to install rofi from source
# install the dependencies
sudo apt install autoconf pkg-config flex bison
sudo apt install libpango1.0-dev libpangocairo-1.0-0 libcairo2-dev libcairomm-1.0-dev libglib2.0-dev librsvg2-dev libstartup-notification0-dev libxcb-xkb-dev libxcb-randr0-dev libxcb-xinerama0-dev libxcb-util-de libxcb-xrm-dev
git clone https://github.com/DaveDavenport/rofi.git
cd rofi/
git submodule update --init
autoreconf -i
mkdir build
cd build
./configure
make
sudo make install
cd ~

# for fish
sudo add-apt-repository ppa:fish-shell/release-2
sudo apt-get update
sudo apt-get install fish 

# for ranger
git clone https://github.com/ranger/ranger.git
cd ranger
sudo make install
cd ~

# for terminology
sudo add-apt-repository ppa:enlightenment-git/ppa
sudo apt-get update
sudo apt-get install terminology

# set up git repos
echo ""
echo "Cloning into git repositories..."
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
git clone https://gitlab.com/thallia/dotconfig.git

# move old configs, but don't remove jic
echo ""
echo "Moving all old configuration files in preparation for the awesome ones."
prefix='old'
config_files=(".vimrc"
		".tmux.conf")
cd ~
for file in ${config_files[*]}
do
	if [-e "$file"]
	then
		echo "moving \"$file\""
		mv "$file" "$prefix$file"
	else
		echo ""\"$file\"" not found, skipping..."
	fi
done

# Symlink ALL THE CONFIGS
echo ""
echo "Linking all the configurations..."
mkdir ~/.config/rofi/
ln -s ~/dotconfig/rofi/rofi-config ~/.config/rofi/config
ln -s ~/dotconfig/vim/vimrc ~/.vimrc
ln -s ~/dotconfig/tmux/tmux.conf ~/.tmux.conf

# Symlink only the useful fish functions
mkdir ~/.config/fish/
mkdir ~/.config/fish/functions/
ln -s ~/dotconfig/fish/functions/:q.fish ~/.config/fish/functions/:q.fish
ln -s ~/dotconfig/fish/functions/:tabnew.fish ~/.config/fish/functions/:tabnew.fish
ln -s ~/dotconfig/fish/functions/:tabn.fish ~/.config/fish/functions/:tabn.fish

# Ask if tmuxinator is wanted
echo ""
read -p "Would you like to install tmuxinator, the tmux project manager? [y/n]" answer
if [ $answer == "y" ]
then
	echo "Installing tmuxinator."
	sudo gem install tmuxinator
	mkdir ~/.config/fish/completions/
	ln -s ~/dotconfig/fish/completions/tmuxinator.fish ~/.config/fish/completions/tmuxinator.fish
else
	echo "Ignoring tmuxinator."
fi

# see if user wants to do terminology
#echo ""
#read -p "gnome-default-terminal is the default terminal, would you like to change it to terminology?
#	 If you would like to have ranger and w3m preview images, this is recommended. [y/n]" answer
#if [ $answer == 'y' ]
#then
#	echo "Setting terminology as default terminal emulator."
#	sudo update-alternatives --config x-terminal-emulator
#else
#	echo "Using gnome-default-terminal."
#fi

echo ""
echo "Setting up fish as the default shell..."
fish_dir=`which fish`
chsh -s $fish_dir
# set fish default welcome message to nothing
set fish_greeting ""
# need to log out and log back in to make changes

# need to figure out why this thing is being a dork
# set up Vim + plugins
echo ""
echo "Compiling YouCompleteMe..."
cd ~/.vim/bundle/YouCompleteMe/
./install.py --clang-completer
cd $currentdir

echo ""
echo "Installing vim plugins..."
echo | vim +PluginInstall +qall

# setting up ranger & w3m for picture previewing
ranger --copy-config=scope
RANGER_LOAD_DEFAULT_RC=false
export $RANGER_LOAD_DEFAULT_RC
ln -s ~/narsil/dotconfig/ranger/rc.conf ~/.config/ranger/rc.conf
# cool, so now we have the scope config and the regular config. 

echo ""
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Done Installing~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "To enable fish as the default shell from now on, please log out and log back in to put changes in effect."
echo ""
