#!/bin/bash

# run from server

echo 'Starting rsync process.'

declare -A ipanduril=( ["gfu"]="" ["home"]=10.0.0.177 )
# first pull from anduril to glamdring
# next pull from narsil to glamdring
# then push to anduril
# then push to narsil

function sync-home {
	/usr/bin/rsync -av thallia@10.0.0.177:narsil/ ~/narsil-server/
	echo "Finished pulling from anduril."
	echo "Pulling from narsil."
	/usr/bin/rsync -av thallia@10.0.0.69:narsil/ ~/narsil-server/
	echo "Pushing to both."
	/usr/bin/rsync -av ~/narsil-server/ thallia@10.0.0.177:narsil/
	/usr/bin/rsync -av ~/narsil-server/ thallia@10.0.0.69:narsil/
	echo "Sync complete."
}


while true; do
	read -p "Are you home? [y/n]"  answer
	# pull from anduril
	case $answer in
	[yY]* ) set anduril = ${ipanduril["home"]}
		sync-home
		exit;;
	
	[nN]* ) read -p "Where are you? " location
		location=${location,,} # lower?
		if [ $location == "gfu" ] 
		then
			echo "Syncing from GFU IP."
			# need to grab that	
		
		elif [ $location == "whitlocks" ]
		then
			echo "Syncing from Whitlock's IP." 
		
		# add entry for sting?
	
		fi
	esac
done
